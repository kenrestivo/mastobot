# mastobot

A WIP experiment with the ostatus/mastodon API, since AP is not yet supported for client-server on pleroma


## License

Copyright © 2018 ken restivo <ken@restivo.org>

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
