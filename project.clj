(defproject mastobot "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [com.taoensso/timbre "4.10.0" :exclusions [org.clojure/tools.reader]]
                 [hiccup "1.0.5"]
                 [mount "0.1.11"]
                 [camel-snake-kebab "0.4.0"]
                 [utilza "0.1.98"]
                 [orchestra "2018.08.19-1"]
                 [org.clojure/core.async "0.4.474"]
                 [org.clojure/tools.nrepl "0.2.13"]
                 [com.sys1yagi/mastodon4j "1.5.0"] 
                 [clj-time "0.14.2"]
                 [cheshire "5.8.0"]
                 [stencil "0.5.0"  :exclusions [org.clojure/core.cache]] 
                 [clj-http "3.7.0"]
                 [org.clojure/tools.trace "0.7.9"]
                 ]
  :main ^:skip-aot mastobot.core
  :target-path "target/%s"
  :repositories [["mastodon" "https://jitpack.io"]]
  :profiles {:uberjar {:aot :all}})
