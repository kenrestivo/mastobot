(ns mastobot.mastodon
  (:require [utilza.log :as ulog]
            [camel-snake-kebab.core :as csk]
            [utilza.core :as utilza]
            [utilza.repl :as urepl]
            [taoensso.timbre :as log])
  (:import  com.google.gson.Gson
            com.sys1yagi.mastodon4j.MastodonClient
            com.sys1yagi.mastodon4j.MastodonClient$Builder
            com.sys1yagi.mastodon4j.api.Scope
            com.sys1yagi.mastodon4j.api.Scope$Name
            com.sys1yagi.mastodon4j.api.entity.auth.AppRegistration
            com.sys1yagi.mastodon4j.api.exception.Mastodon4jRequestException
            com.sys1yagi.mastodon4j.api.method.Apps
            okhttp3.OkHttpClient
            okhttp3.OkHttpClient$Builder
            ))


(defn make-apps
  "Takes a hostname. 
   Returns an apps for it."
  [hostname]
  (let [builderberger (MastodonClient$Builder. hostname (OkHttpClient$Builder.) (Gson.))
        client (.build builderberger )]
    (-> builderberger
        .build
        (Apps.))))


(defn make-scope
  "Returns a scope"
  []
  ;; TODO; multiple arities if needed for different scopes
  (Scope. (into-array [Scope$Name/ALL])))


(defn proper-bean
  [obj]
  (->> obj
       bean
       (utilza/xform-keys csk/->kebab-case-keyword)))


(defn register-app
  "Takes a hostname and an app name. 
   Returns a map with clientId, clientSecret, id, instancename, class, and redirecturl"
  [hostname app-name]
  ;; transliterated from https://github.com/sys1yagi/mastodon4j/blob/master/sample/src/main/java/com/sys1yagi/mastodon4j/sample/GetAppRegistration.java
  (-> hostname
      make-apps
      (.createApp  app-name
                   "urn:ietf:wg:oauth:2.0:oob"
                   (make-scope)
                   "")
      .execute
      proper-bean))


;; TODO: not yet working
(defn get-token
  "Basically login. Takes a map with login creds.
   Returns a map with accessToken"
  [{:keys [instance-name client-id client-secret email-address password]}]
  (-> instance-name
      make-apps
      (.postUserNameAndPassword client-id
                                client-secret
                                (make-scope)
                                email-address
                                password)
      .execute
      proper-bean))

